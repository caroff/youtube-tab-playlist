# Youtube Tab Playlist

When a youtube tab finishes playing its video, stop it, select the next youtube tab and start playing it. Note: you might want to use a window dedicated to youtube tabs so as not to be disturbed by the extension automatically selecting the next tab.

As of version 0.0.2, the extension loops back to first youtube.com/watch tab when the last finishes.

In version 0.0.2, the extension does nothing if there's only one youtube.com/watch tab open.

### Autoplay

If Youtube's autoplay option is enabled, lets the next video be loaded, but stops it from playing. This way, the next time the tab is selected by the extension, it will be a different song.

If Youtube's autoplay option is disabled, the next time the tab is selected, the same song will be played.
